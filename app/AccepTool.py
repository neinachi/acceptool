from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtSql import QSqlQueryModel, QSqlDatabase
from time import strftime
import sqlite3
import openpyxl
import os
import sys


cats = QSqlQueryModel()
subcats = QSqlQueryModel()
rebukes = QSqlQueryModel()
results = QSqlQueryModel()
catclicked = ''
subcatclicked = ''
rebukeclicked = ''
key = ''
selection = ('', '', '', '')
targetfile = None

Form, Window = uic.loadUiType("main.ui")
app = QApplication([])
window = Window()
form = Form()
form.setupUi(window)
try:
    targetfile = sys.argv[1]
except IndexError:
    print('Open this application only with the parameter as a path to VN file (*.xlsm)')
else:
    window.show()
    window.setWindowTitle('AccepTool (' + ''.join(targetfile.split('\\')[-1]) + ')')
    form.respcomboBox.addItems(['ООО «Техкомпания Хуавэй»', 'ПАО «МТС»'])
    form.prioritycomboBox.addItems(['A', 'B'])
    form.resultstable.setModel(results)
    form.cattable.setModel(cats)
    form.subcattable.setModel(subcats)
    form.rebuketable.setModel(rebukes)

    db = QSqlDatabase.addDatabase('QSQLITE')
    db.setDatabaseName("accept.db")
    db.open()
    con = sqlite3.connect('accept.db')
    cursor = con.cursor()
    cursor.execute("PRAGMA case_sensitive_like = 0")
    cursor.execute("PRAGMA main.auto_vacuum = 1")
    cursor.execute("CREATE TABLE IF NOT EXISTS categories("
                   "cat text, "
                   "subcat text,"
                   "rebuke text, "
                   "priority text, "
                   "resp text, "
                   "keys text)")
    cursor.execute("CREATE TABLE IF NOT EXISTS results("
                   "id INTEGER PRIMARY KEY, "
                   "cat text, "
                   "subcat text, "
                   "rebuke text, "
                   "priority text, "
                   "resp text, "
                   "remark text, "
                   "total text, "
                   "numberincat integer)")
    cursor.execute('DELETE FROM results')

    try:
        con.commit()
    except:
        pass

    book = openpyxl.open(targetfile, read_only=False, keep_vba=True)
    sheet = book['Лист1']


def takerebukesfromvn(category, minrow, maxrow):
    global sheet
    for row in sheet.iter_rows(min_row=minrow, max_row=maxrow):
        if row[1].value is not None:
            subcatrebuke = row[1].value.replace('Подкатегория: ', '&&&').replace('\nЗамечание: ', '&&&').split('&&&')
            countincat = cursor.execute("SELECT COUNT (*) FROM results WHERE cat = ?",
                                         (category, )).fetchone()[0]
            if row[6].value is not None:
                remark = row[6].value
            else:
                remark = ''
            cursor.execute("INSERT INTO results(cat, subcat, rebuke, priority, resp, remark, total, numberincat) "
                           "VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                           (category,
                            subcatrebuke[1],
                            subcatrebuke[2],
                            row[2].value,
                            row[4].value,
                            row[6].value,
                            'Подкатегория:\n' + subcatrebuke[1] + '\n' +
                            'Замечание:\n' + subcatrebuke[2] + '\n' +
                            'Уточнение:\n' + remark + '\n' +
                            'Приоритет:\n' + row[2].value + '\n' +
                            'Зона ответственности:\n' + row[4].value,
                            countincat + 1))
        try:
            con.commit()
        except:
            pass


def afterkeylineedit(line):
    global cats, subcats, rebukes, key, catclicked, subcatclicked, rebukeclicked
    key = line
    cats.setQuery("SELECT DISTINCT "
                  "cat AS 'Категория' "
                  "FROM categories "
                  "WHERE rebuke LIKE '%{0}%' "
                  "OR keys LIKE '%{0}%'".format(key))
    subcats.setQuery("SELECT DISTINCT "
                     "subcat AS 'Подкатегория' "
                     "FROM categories "
                     "LIMIT 0")
    rebukes.setQuery("SELECT "
                     "rebuke AS 'Замечание' "
                     "FROM categories "
                     "WHERE rebuke LIKE '%{2}%' OR keys LIKE '%{2}%'".format(catclicked, subcatclicked, key))
    catclicked = ''
    subcatclicked = ''
    rebukeclicked = ''
    form.catlabel.setText('Категория: ')
    form.subcatlabel.setText('Подкатегория: ')
    form.rebukelabel.setText('Замечание: ')


def fillfilters():
    global cats, subcats, rebukes
    cats.setQuery("SELECT DISTINCT "
                  "cat AS 'Категория' "
                  "FROM categories ")
    subcats.setQuery("SELECT DISTINCT "
                     "subcat AS 'Подкатегория' "
                     "FROM categories "
                     "LIMIT 0")
    rebukes.setQuery("SELECT "
                     "rebuke AS 'Замечание' "
                     "FROM categories ")
    results.setQuery("SELECT cat AS 'Категория', numberincat AS '№', total AS 'Замечание' FROM results "
                     "ORDER BY cat, numberincat")
    form.resultstable.resizeRowsToContents()
    form.resultstable.resizeColumnsToContents()


def reimportrebukes():
    try:
        rebukebook = openpyxl.open('../import\\Замечания.xlsx', read_only=True)
    except:
        print('Файл "Замечания.xlsx" не открылся, он должен быть в папке "import".')
    else:
        try:
            rebukesheet = rebukebook['Замечания']
        except:
            print('В файле "Замечания.xlsx" нет вкладки "Замечания".')
        else:
            cursor.execute('DELETE FROM categories')
            rowcounter = 2
            rowsamount = rebukesheet.max_row
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка списка замечаний из файла "Замечания.xlsx" начата...')
            for row in rebukesheet.iter_rows(min_row=2):
                print(f' [{strftime("%H:%M:%S")}]'
                      f' Cтрока: {rowcounter}/{rowsamount}', end='\r')
                rowcounter += 1
                if row[0].value is not None:
                    cursor.execute("INSERT INTO categories(cat, subcat, rebuke, priority, resp, keys) "
                                   "VALUES(?, ?, ?, ?, ?, ?)",
                                   (row[0].value,
                                    row[1].value,
                                    row[2].value,
                                    row[3].value,
                                    row[4].value,
                                    row[5].value))
            con.commit()
            book.close()
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка списка замечаний из файла "Замечания.xlsx" завершена.                                ')
    fillfilters()


def clickcattable(index):
    global catclicked, subcatclicked, rebukeclicked, key
    catclicked = form.cattable.model().data(index, Qt.DisplayRole)
    subcatclicked = ''
    rebukeclicked = ''
    subcats.setQuery("SELECT DISTINCT "
                     "subcat AS 'Подкатегория' "
                     "FROM categories "
                     "WHERE cat = '{0}' "
                     "AND (rebuke LIKE '%{1}%' OR keys LIKE '%{1}%')".format(catclicked, key))
    rebukes.setQuery("SELECT "
                     "rebuke AS 'Замечание' "
                     "FROM categories "
                     "WHERE cat = '{0}' "
                     "AND (rebuke LIKE '%{1}%' OR keys LIKE '%{1}%')".format(catclicked, key))
    form.catlabel.setText('Категория: ' + catclicked)
    form.subcatlabel.setText('Подкатегория: ')
    form.rebukelabel.setText('Замечание: ')


def clicksubcattable(index):
    global catclicked, subcatclicked, rebukeclicked
    subcatclicked = form.subcattable.model().data(index, Qt.DisplayRole)
    rebukeclicked = ''
    rebukes.setQuery("SELECT "
                     "rebuke AS 'Замечание' "
                     "FROM categories "
                     "WHERE cat = '{0}' "
                     "AND subcat = '{1}' "
                     "AND (rebuke LIKE '%{2}%' OR keys LIKE '%{2}%')".format(catclicked, subcatclicked, key))
    form.catlabel.setText('Категория: ' + catclicked)
    form.subcatlabel.setText('Подкатегория: ' + subcatclicked)
    form.rebukelabel.setText('Замечание: ')


def makeselection():  # cat, subcat, rebuke, priority, resp
    global catclicked, subcatclicked, rebukeclicked, selection
    selection = cursor.execute("SELECT "
                               "cat, subcat, rebuke, priority, resp "
                               "FROM categories "
                               "WHERE rebuke = '{0}' "
                               "AND cat LIKE '%{1}' "
                               "AND subcat LIKE '%{2}'".format(rebukeclicked, catclicked, subcatclicked)).fetchall()


def clickrebuketable(index):
    global catclicked, subcatclicked, rebukeclicked, selection
    rebukeclicked = form.rebuketable.model().data(index, Qt.DisplayRole)
    makeselection()  # cat, subcat, rebuke, priority, resp
    if len(selection) == 1:
        form.catlabel.setText('Категория: ' + selection[0][0])
        form.subcatlabel.setText('Подкатегория: ' + selection[0][1])
        form.rebukelabel.setText('Замечание: ' + selection[0][2])
        form.prioritycomboBox.setCurrentText(selection[0][3])
        form.respcomboBox.setCurrentText(selection[0][4])
    else:
        if catclicked == '' or subcatclicked == '':
            rebukeclicked = ''
            form.rebukelabel.setText('Замечание: ')
            msg = QMessageBox()
            msg.setWindowTitle("Неточность...")
            msg.setText("Это замечание есть в разных категориях.\nВыберите категорию и подкатегорию.")
            x = msg.exec_()


def doubleclickrebuketable(index):
    clickrebuketable(index)
    addrebuke()


def addrebuke():
    global rebukeclicked, results, selection
    if selection[0][0] != '' and selection[0][1] != '' and rebukeclicked != '':
        numberincat = cursor.execute("SELECT COUNT (*) FROM results WHERE cat = ?", (selection[0][0], )).fetchone()[0]
        cursor.execute("INSERT INTO results(cat, subcat, rebuke, priority, resp, remark, total, numberincat) "
                       "VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                       (selection[0][0],
                        selection[0][1],
                        selection[0][2],
                        form.prioritycomboBox.currentText(),
                        form.respcomboBox.currentText(),
                        form.textEdit.toPlainText(),
                        'Подкатегория:\n' + selection[0][1] + '\n' +
                        'Замечание:\n' + rebukeclicked + '\n' +
                        'Уточнение:\n' + form.textEdit.toPlainText() + '\n' +
                        'Приоритет:\n' + form.prioritycomboBox.currentText() + '\n' +
                        'Зона ответственности:\n' + form.respcomboBox.currentText(),
                        numberincat + 1))
        cursor.execute("COMMIT")
        results.setQuery("SELECT cat AS 'Категория', numberincat AS '№', total AS 'Замечание' FROM results "
                         "ORDER BY cat, numberincat")
        form.resultstable.resizeRowsToContents()
        form.resultstable.resizeColumnsToContents()
        form.prioritycomboBox.setCurrentIndex(0)
        form.respcomboBox.setCurrentIndex(0)
        form.textEdit.clear()


def clearfilters():
    global cats, subcats, rebukes, catclicked, subcatclicked, rebukeclicked, key
    cats.setQuery("SELECT DISTINCT "
                  "cat AS 'Категория' "
                  "FROM categories ")
    subcats.setQuery("SELECT DISTINCT "
                     "subcat AS 'Подкатегория' "
                     "FROM categories "
                     "LIMIT 0")
    rebukes.setQuery("SELECT "
                     "rebuke AS 'Замечание' "
                     "FROM categories ")
    form.keylineEdit.clear()
    form.catlabel.setText('Категория: ')
    form.subcatlabel.setText('Подкатегория: ')
    form.rebukelabel.setText('Замечание: ')
    catclicked = ''
    subcatclicked = ''
    rebukeclicked = ''
    key = ''


def rebukeputtovn(headerrow, countincat, rebuke):
    global sheet
    targetrow = headerrow + countincat
    sheet.cell(row=headerrow, column=14).value = targetrow + 1
    sheet.row_dimensions[targetrow].hidden = False
    sheet.row_dimensions[targetrow].height = 50
    sheet.cell(row=targetrow, column=2).value = 'Подкатегория: ' + rebuke[1] + '\n' + \
                                                'Замечание: ' + rebuke[2]
    sheet.cell(row=targetrow, column=3).value = rebuke[3]
    sheet.cell(row=targetrow, column=5).value = rebuke[4]
    sheet.cell(row=targetrow, column=7).value = rebuke[5]
    sheet.cell(row=targetrow, column=17).value = str(os.environ["USERNAME"])


def clearvnrow(rowtoclear):
    global sheet
    if sheet.cell(row=rowtoclear, column=2).value is not None:
        sheet.row_dimensions[rowtoclear].hidden = True
        sheet.cell(row=rowtoclear, column=2).value = None
        sheet.cell(row=rowtoclear, column=3).value = None
        sheet.cell(row=rowtoclear, column=5).value = None
        sheet.cell(row=rowtoclear, column=7).value = None
        sheet.cell(row=rowtoclear, column=17).value = None


def reportcomplete():
    global sheet
    rebukestuple = cursor.execute("SELECT cat, subcat, rebuke, priority, resp, remark FROM results").fetchall()
    radrelaycounter = 0
    garbagecounter = 0
    antennacounter = 0
    alarmscounter = 0
    powercounter = 0
    towercounter = 0
    roomcounter = 0
    docscounter = 0

    if targetfile is not None:

        for rebuke in rebukestuple:

            if rebuke[0] == 'Состояние':
                garbagecounter += 1
                rebukeputtovn(10, garbagecounter, rebuke)

            elif rebuke[0] == 'ЭПУ':
                powercounter += 1
                rebukeputtovn(111, powercounter, rebuke)

            elif rebuke[0] == 'РРЛ':
                radrelaycounter += 1
                rebukeputtovn(212, radrelaycounter, rebuke)

            elif rebuke[0] == 'АМС':
                towercounter += 1
                rebukeputtovn(313, towercounter, rebuke)

            elif rebuke[0] == 'АФУ':
                antennacounter += 1
                rebukeputtovn(414, antennacounter, rebuke)

            elif rebuke[0] == 'БС':
                roomcounter += 1
                rebukeputtovn(515, roomcounter, rebuke)

            elif rebuke[0] == 'ОПС':
                alarmscounter += 1
                rebukeputtovn(717, alarmscounter, rebuke)

            elif rebuke[0] == 'Документы':
                docscounter += 1
                rebukeputtovn(818, docscounter, rebuke)

        for rowid in range(11 + garbagecounter, 111):
            clearvnrow(rowid)
        for rowid in range(112 + powercounter, 212):
            clearvnrow(rowid)
        for rowid in range(213 + radrelaycounter, 313):
            clearvnrow(rowid)
        for rowid in range(314 + towercounter, 414):
            clearvnrow(rowid)
        for rowid in range(415 + antennacounter, 515):
            clearvnrow(rowid)
        for rowid in range(516 + roomcounter, 616):
            clearvnrow(rowid)
        for rowid in range(718 + alarmscounter, 818):
            clearvnrow(rowid)
        for rowid in range(819 + docscounter, 919):
            clearvnrow(rowid)

        book.save(targetfile)

        print(f' [{strftime("%H:%M:%S")}] Изменения внесены в файл: {targetfile}')
        msg = QMessageBox()
        msg.setWindowTitle('Ведомость готова')
        msg.setText(f'Изменения внесены в файл:\n{targetfile}')
        x = msg.exec_()


def resultchange(text):
    global catclicked, subcatclicked, rebukeclicked, selection
    tochange = cursor.execute("SELECT cat, subcat, rebuke, priority, resp, remark "
                              "FROM results "
                              "WHERE total = ?", (form.resultstable.model().data(text, Qt.DisplayRole), )).fetchone()
    if tochange is not None:
        clearfilters()
        catclicked = tochange[0]
        subcatclicked = tochange[1]
        rebukeclicked = tochange[2]
        makeselection()  # cat, subcat, rebuke, priority, resp
        form.catlabel.setText('Категория: ' + selection[0][0])
        form.subcatlabel.setText('Подкатегория: ' + selection[0][1])
        form.rebukelabel.setText('Замечание: ' + selection[0][2])
        form.prioritycomboBox.setCurrentText(tochange[3])
        form.respcomboBox.setCurrentText(tochange[4])
        form.textEdit.setText(tochange[5])
        cursor.execute("DELETE FROM results WHERE total = ?", (form.resultstable.model().data(text, Qt.DisplayRole), ))
        cursor.execute("COMMIT")
        results.setQuery("SELECT cat AS 'Категория', numberincat AS '№', total AS 'Замечание' FROM results "
                         "ORDER BY cat, numberincat")


form.action_reimportrebukes.triggered.connect(reimportrebukes)
form.cattable.clicked.connect(clickcattable)
form.subcattable.clicked.connect(clicksubcattable)
form.rebuketable.clicked.connect(clickrebuketable)
form.rebuketable.doubleClicked.connect(doubleclickrebuketable)
form.keylineEdit.textChanged[str].connect(afterkeylineedit)
form.clearbutton.clicked.connect(clearfilters)
form.addrebukebutton.clicked.connect(addrebuke)
form.reportcompletebutton.clicked.connect(reportcomplete)
form.resultstable.doubleClicked.connect(resultchange)

if targetfile is not None:
    takerebukesfromvn('Состояние', 11, 110)
    takerebukesfromvn('ЭПУ', 112, 211)
    takerebukesfromvn('РРЛ', 213, 312)
    takerebukesfromvn('АМС', 314, 413)
    takerebukesfromvn('АФУ', 415, 514)
    takerebukesfromvn('БС', 516, 615)
    takerebukesfromvn('ОПС', 718, 817)
    takerebukesfromvn('Документы', 819, 918)
    results.setQuery("SELECT cat AS 'Категория', numberincat AS '№', total AS 'Замечание' FROM results "
                     "ORDER BY cat, numberincat")
    form.resultstable.resizeRowsToContents()
    form.resultstable.resizeColumnsToContents()
    form.prioritycomboBox.setCurrentIndex(0)
    form.respcomboBox.setCurrentIndex(0)
    form.textEdit.clear()

    fillfilters()
    app.exec()

else:
    from win32com.client import Dispatch
    import winshell
    shell = Dispatch('WScript.Shell')
    shortcut = shell.CreateShortCut(''.join([winshell.sendto(), '\\AccepTool.lnk']))
    shortcut.Targetpath = ''.join([os.getcwd(), '\\AccepTool.exe'])
    shortcut.WorkingDirectory = os.getcwd()
    shortcut.save()
    print('Программа добавлена в контекстное меню для файлов. Можно запускать и через контекстное меню "Отправить" '
          'на файлах ведомостей (*.xlsm)')
    input("Press Enter to exit...")
